package com.strategypattern.helper;

import com.strategypattern.beans.HTMLMessageFormatterImpl;
import com.strategypattern.beans.IMessageFormatter;
import com.strategypattern.beans.PDFMessageFormatterImpl;

public class MessageFormatterFactory {
	public static IMessageFormatter createMessageFormatter(String type) {
		IMessageFormatter messageFormatter = null;

		if (type.equals("html")) {
			messageFormatter = new HTMLMessageFormatterImpl();
		} else if (type.equals("pdf")) {
			messageFormatter = new PDFMessageFormatterImpl();
		}
		return messageFormatter;
	}
}
