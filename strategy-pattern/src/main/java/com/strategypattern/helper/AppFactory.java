package com.strategypattern.helper;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class AppFactory {
	public static Object createObject(String lClassname) throws Exception {
		Properties props = null;
		String fqnClass = null;
		Class<?> clazz = null;
		Object obj = null;

		props = new Properties();
		props.load(new FileInputStream(new File(
				"D:\\work\\master\\spring\\20201001\\core\\strategy-pattern\\src\\main\\resources\\appClasses.properties")));
		if (props.containsKey(lClassname) == false) {
			throw new Exception("Class name not found for given logical class name");
		}
		fqnClass = props.getProperty(lClassname);
		clazz = Class.forName(fqnClass);
		obj = clazz.newInstance();

		return obj;
	}
}
