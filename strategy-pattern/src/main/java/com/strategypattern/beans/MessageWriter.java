package com.strategypattern.beans;

public class MessageWriter {
	private IMessageFormatter messageFormatter;

	public void writeMessage(String message) {
		String formattedMessage = null;

		// messageFormatter = new HTMLMessageFormatterImpl();
		// it is exposed to concreate classname and complexity of creating object
		// messageFormatter = MessageFormatterFactory.createMessageFormatter("pdf");

		formattedMessage = messageFormatter.formatMessage(message);
		System.out.println(formattedMessage);
	}

	public void setMessageFormatter(IMessageFormatter messageFormatter) {
		this.messageFormatter = messageFormatter;
	}

}
