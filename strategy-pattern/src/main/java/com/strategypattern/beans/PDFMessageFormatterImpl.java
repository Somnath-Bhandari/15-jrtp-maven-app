package com.strategypattern.beans;

public class PDFMessageFormatterImpl implements IMessageFormatter {
	@Override
	public String formatMessage(String message) {
		String formattedMessage = null;

		formattedMessage = "<PDF>" + message + "</PDF>";
		return formattedMessage;
	}
}
