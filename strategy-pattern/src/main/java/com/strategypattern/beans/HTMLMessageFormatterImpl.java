package com.strategypattern.beans;

public class HTMLMessageFormatterImpl implements IMessageFormatter {
	@Override
	public String formatMessage(String message) {
		String formattedMessage = null;

		formattedMessage = "<HTML><BODY>" + message + "</BODY></HTML>";
		return formattedMessage;
	}
}
