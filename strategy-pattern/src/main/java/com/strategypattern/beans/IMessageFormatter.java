package com.strategypattern.beans;

public interface IMessageFormatter {
	String formatMessage(String message);
}
