package com.strategypattern.test;

import com.strategypattern.beans.IMessageFormatter;
import com.strategypattern.beans.MessageWriter;
import com.strategypattern.helper.AppFactory;

public class StrategyPatternTest {
	public static void main(String[] args) throws Exception {
		//MessageWriter messageWriter = new MessageWriter();
		//IMessageFormatter messageFormatter = new HTMLMessageFormatterImpl();
		MessageWriter messageWriter = (MessageWriter) AppFactory.createObject("messageWriter");
		IMessageFormatter messageFormatter = (IMessageFormatter) AppFactory.createObject("messageFormatter");
		
		messageWriter.setMessageFormatter(messageFormatter);
		messageWriter.writeMessage("Welcome to Strategy Design Pattern");
	}
}
